// Libraries
import React from "react";

// UI Elements
import { MDBRow, MDBCol, MDBCardBody, MDBCardText, MDBCard } from "mdbreact";

// Components
import NutrientComp from '../NutrientComp';

// Helper Functions
import { makeID } from '../../utils/helper'; // Function to Help Generate Unique ID's 

// This component is used for the food card and is a sub component of the main screen.
export default class CardComp extends React.Component {
  render() {
    const { data } = this.props;
    const { name, weight, measure, nutrients} = data;
    return (
      // Show or hide element
      // Map options from JSON Schema
      <MDBCol className={`mt-5 mx-3 mx-lg-0 py-3 px-2 col-lg-4 col-12`}>
      <MDBCard style={{ width: "22rem" }}>
        <MDBCardBody>
          <h4 className="text-center">{name}</h4>
            <MDBRow className="d-flex flex-column text-center">
             
                <p className="pb-0 mb-0">Weight: {weight}</p>
                <p className="pb-0 mb-0">Measure: {measure}</p>
        
             
            </MDBRow>
            <hr/>
            <h5>Nutrient Metrics</h5>
            {/* The nutrient array is mapped here to display that metrics. */}
            {nutrients.map(nutrient => {
                           return (
               <NutrientComp key={makeID()} data={nutrient}/>
             ) })}
        </MDBCardBody>
      </MDBCard>
    </MDBCol>
    );
  }
}