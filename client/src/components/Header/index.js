// Libraries
import React, { Component } from "react";

// UI Elements
import { MDBContainer, MDBRow, MDBCol } from "mdbreact";

// Header of the app. 
class HeaderComp extends Component {
  render() {
    const { title } = this.props;
    return (
      <div>
        <MDBContainer fluid className="ml-0 p-0 secondary-color-dark white-text">
          <MDBRow>
            <MDBCol className="text-center" size="12">
              <h1 className="py-2">{title}</h1>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </div>
    );
  }
}
export default HeaderComp;
