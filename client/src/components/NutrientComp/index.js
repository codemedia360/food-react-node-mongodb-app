// Libraries
import React from "react";

// UI Elements
import { MDBRow } from "mdbreact";

// This component lists the nutrient metrics.
export default class Nutrient extends React.Component {
  render() {
    const { data } = this.props;
    const { nutrient, unit, gm } = data;
    return (
      <MDBRow className="d-flex flex-column text-left">
        <p className="pb-0 px-3 mb-0  font-weight-bold">{nutrient}<span className="ml-3 font-weight-normal">{`${gm} ${unit}`}</span></p>
      </MDBRow>
    );
  }
}