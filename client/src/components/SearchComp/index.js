import React from 'react'
import { MDBContainer } from 'mdbreact';

export default class SearchComp extends React.Component {
  render() {
    // These props/component handle the processing and loading the search element on the screen.
    const { label, handleChange, value, name } = this.props;
    return (
      <MDBContainer className={`mt-5`}>
        <h1>{label}</h1>
        <div className="form-group">
          <input name={name} value={value} onChange={handleChange} className="form-control" placeholder="Search here for foods." autoFocus={value !== "" ? true : false} onFocus={(e) => {
            e.target.value = ''; e.target.value = value;
          }}></input>
        </div>
      </MDBContainer>
    )
  }
}
