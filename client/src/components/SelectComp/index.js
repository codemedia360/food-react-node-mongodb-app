// Libraries
import React from "react";

// UI Elements
import { MDBCol } from "mdbreact";

export default class SelectComp extends React.Component {
  render() {
    // These props/component handle the processing and loading of all the select elements on the screen.
    const { label, handleChange, value, name } = this.props;
    return (
      <MDBCol className={`mt-5 col-md-3 col-12 center-block mx-auto`}>
        <h5 className="mb-3">{label}</h5>
        <select name={name} value={value} onChange={handleChange} className="browser-default custom-select w-50">
          <option value="0">Choose an option</option>
          <option value="0">0</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="5">5</option>
          <option value="50">50</option>
          <option value="100">100</option>
          <option value="200">200</option>
        </select>
      </MDBCol>
    );
  }
}