// External dependencies
import React from 'react';
import ReactDOM from 'react-dom';
import Aviator from 'aviator';

// Internal dependencies
import './index.css';
import * as serviceWorker from './serviceWorker';

// Screens
import MainScreen from "./script/screens/main.screen";

//MDB Styles
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css"

// Screen Setup
const MainTarget = {
    screen: () => {
        ReactDOM.render(<MainScreen />, document.getElementById('root'));
    }
};

// This is where you set routes
Aviator.setRoutes({
    target: MainTarget,
    '/*': 'screen',
    '/': 'screen'
});

Aviator.dispatch();

serviceWorker.unregister();
