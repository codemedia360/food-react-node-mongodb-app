// Libraries
import React from 'react';

// Components
import Header from '../../components/Header';
import SelectComp from '../../components/SelectComp';
import SearchComp from '../../components/SearchComp';
import CardComp from '../../components/CardComp';

// UI Elements 
import { MDBContainer, MDBAlert, MDBIcon, MDBRow, MDBBtn } from 'mdbreact';

// Config file
import config from '../../config/config';

// Services
import { apiGetRequest } from '../services/restAPI';

// Helper Functions
import { makeID } from '../../utils/helper'; // Function to Help Generate Unique ID's 

// Global Variables
const API_URL = config.API_URL;
// This array is used to generate the select items.
const formEl = ["Alcohol, (Grams)", "Protein, (Grams)", "Fat, (Grams)", "Carbohydrate, (Grams)", "Sugar, (Grams)", "Ash, (Grams)", "Energy, (Kcal)", "Fiber, (Grams)", "Search"];


class MainScreen extends React.Component {
  constructor(props) {
    super(props);
    // Alerts, JSON Schema, Form Values and Functions 
    this.state = {
      alertType: "success",
      alertText: "",
      toggleAlert: false,
      foodData: [],
      activeNutrients: [],
      alcohol: "",
      protein: "",
      fat: "",
      carbohydrate: "",
      sugar: "",
      ash: "",
      energy: "",
      fiber: "",
      search: "",
      start: 0,
      length: 40,

    }
  }


  componentDidMount = async () => {
    // Initial Get Request To Server To Get JSON Schema
    await this.getFoodData("all");
  }

  getFoodData = async (type) => {
    const { start, length, activeNutrients, search, alcohol, protein, fat, carbohydrate, sugar, ash, energy, fiber } = this.state;
    let endpoint = `${API_URL}/v1/foods?start=${start}&length=${length}`;
      // The query is modified depending on the parameters assigned by the user. 
    for (let i in activeNutrients) {
      switch (activeNutrients[i]) {
        case "search":
          search !== "" ? endpoint += `&search=${search.trim()}` : endpoint += "";
          break;
        case "alcohol":
          endpoint += `&alcohol=${alcohol.trim()}`
          break;
        case "sugar":
          endpoint += `&sugar=${sugar.trim()}`
          break;
        case "protein":
          endpoint += `&protein=${protein.trim()}`
          break;
        case "fat":
          endpoint += `&lipid=${fat.trim()}`
          break;
        case "carbohydrate":
          endpoint += `&carbs=${carbohydrate.trim()}`
          break;
        case "energy":
          endpoint += `&energy=${energy.trim()}`
          break;
        case "ash":
          endpoint += `&ash=${ash.trim()}`
          break;
        case "fiber":
          endpoint += `&fiber=${fiber.trim()}`
          break;
        default:
          console.log("Something went wrong with Switch")
          break;
      }
    }
    // This  get request hits api/foods with different configurations of paramters to return a list of foods.
    const data = await apiGetRequest(endpoint);
    // The response gets saved in state. 
    this.setState({ foodData: data.data });
  }

  // Alert Function That Handles All Alerts In The APP
  showAlert = (alertType, alertText) => {
    this.setState({ alertType: alertType, alertText: alertText, toggleAlert: true });
    setTimeout(() => {
      this.setState({ alertType: "", alertText: "", toggleAlert: false });
    }, 4000);
  }

  // This functions handles all the change values for the elements on the screen. 
  handleChange = async (e) => {
    const { activeNutrients } = this.state;
    const name = e.target.name;
    const value = e.target.value;
    const nutrientArr = activeNutrients;
     // Is the value of what the user is trying to find present or not. 
    if (nutrientArr.indexOf(name) === -1) {
      // The value has never been changed in this session so add it to the nutrient array. 
      nutrientArr.push(e.target.name)
      await this.setState({ activeNutrients: nutrientArr });
    }

    await this.setState({ [name]: value });
    // Data refresh
    this.getFoodData(name);
  }


  handleClick = () => {
    // The laod more button doubles the length value in the DB query evertime it is pressed. 
    const { length } = this.state;
    const newLength = length + length;
    this.setState({ length: newLength })
    this.showAlert("danger", "Loading More Now")
    setTimeout(() => {
      // Refresh data.
      this.getFoodData("all");
    }, 2000);
  }



  render() {
    const { toggleAlert, alertType, alertText, foodData } = this.state;
    let data = foodData.data;
    return (
      <div id='main-screen'>
        <MDBContainer fluid className="ml-0 p-0 fixed-top text-center">
          <MDBAlert className={`${toggleAlert ? "d-block" : "d-none"}`} color={alertType === "success" ? "success" : "danger"} >
            <MDBIcon icon={alertType === "success" ? "check-circle" : "exclamation-circle"} className="mr-2" />
            {alertText}
          </MDBAlert>
        </MDBContainer>
        <Header title="Poo to Pill" />
        <h1 className="text-center mt-5 px-2">Find The Food That Will Help Your Poo</h1>
        <p className="text-center mt-3 px-2">You can use the search to find your food or select from the nutrition search options below. </p>
        <MDBContainer>
          <MDBRow>
            {/* Depending on the value of the element it will generate a search element or a select element. */}
            {formEl.map(el => {
              return el === "Search" ?
                <SearchComp key={makeID()} name={el.split(",")[0].toLocaleLowerCase()} value={this.state[el.split(",")[0].toLocaleLowerCase()]} label={el} handleChange={this.handleChange} />
                :
                <SelectComp key={makeID()} name={el.split(",")[0].toLocaleLowerCase()} value={this.state[el.split(",")[0].toLocaleLowerCase()]} label={el} handleChange={this.handleChange} />
            })}
          </MDBRow>
        </MDBContainer>
        <MDBContainer>
          <MDBRow className="d-flex justify-content-center">
            <p className="mb-0 pb-0">Viewing {foodData.resultCount} Results</p>
          </MDBRow>
        </MDBContainer>
        <MDBContainer>
          <MDBRow>
            {data ?
              data.map(val => {
                return (
                  <CardComp key={makeID()} data={val} />
                )
              })
              :
              <h1>Loading....</h1>
            }
          </MDBRow>
          <MDBContainer className="pb-5">
            <MDBRow className="d-flex justify-content-center mt-4">
              <MDBBtn color="primary" onClick={this.handleClick}>Load More</MDBBtn>
            </MDBRow>
          </MDBContainer>
        </MDBContainer>
      </div>
    );
  }
};

export default MainScreen;