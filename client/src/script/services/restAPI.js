// Libraries
import axios from 'axios';

// These servers are used for all http requests needed in the app.

// API GET Request Function
export const apiGetRequest = async (apiEndpoint) => {
    try {
        return await axios.get(apiEndpoint);
    }
    catch (error) {
        return console.log(error)
    }
}

// API POST Request Function
export const apiPostRequest = async (apiEndpoint, dataT) => {
    try {
        return await axios.post(apiEndpoint, dataT)
            .then((res) => {
                if (res.data !== null || undefined) {
                    return res.data

                }
            })
            .catch(error => {
                return error.response.data
            });
    }
    catch (error) {
        //.log(error)
        return error
    }
}
