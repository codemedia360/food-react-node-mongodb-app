
// This function returns a unique id. 
export function makeID(){
    try {
        var text = "";
        var possible =
          "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 40; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }
    catch (error) {
        console.error(error);
    }
}

  