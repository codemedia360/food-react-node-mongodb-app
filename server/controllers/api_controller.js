const mongoose = require('mongoose'),
  Food = mongoose.model('Foods');

module.exports = {
  getFoodData: async (req, res) => {
    try {

      const q = {};
      const qParameters = req.query;
      let matchArr = [];
      // The below conditionals are responsible for what query gets made to the db. Then returns that specific queries data.
      if ('search' in qParameters) {
        q.name = new RegExp(qParameters.search, 'i');
      }

      if ('pagination' in qParameters) {
        q.name = new RegExp(qParameters.search, 'i');
      }

      if ('sugar' in qParameters) {
        matchArr.push( { "$elemMatch": { nutrient_id: '269', gm: { $lte: parseFloat(qParameters.sugar) } } });
      }

      if ('alcohol' in qParameters) {
        matchArr.push({ "$elemMatch": { nutrient_id: '221', gm: { $lte: parseFloat(qParameters.alcohol) } } });
      }

      if ('protein' in qParameters) {
        matchArr.push( { "$elemMatch": { nutrient_id: '203', gm: { $lte: parseFloat(qParameters.protein) } } });
      }

      if ('lipid' in qParameters) {
        matchArr.push( { "$elemMatch": { nutrient_id: '204', gm: { $lte: parseFloat(qParameters.lipid) } } });
      }

      if ('carbs' in qParameters) {
        matchArr.push( { "$elemMatch": { nutrient_id: '205', gm: { $lte: parseFloat(qParameters.carbs) } } });
      }

      if ('ash' in qParameters) {
        matchArr.push( { "$elemMatch": { nutrient_id: '207', gm: { $lte: parseFloat(qParameters.ash) } } });
      }

      if ('energy' in qParameters) {
        matchArr.push( { "$elemMatch": { nutrient_id: '208', gm: { $lte: parseFloat(qParameters.energy) } } });
      }

      if ('fiber' in qParameters) {
          matchArr.push( { "$elemMatch": { nutrient_id: '291', gm: { $lte: parseFloat(qParameters.fiber) } } });
      }

      if ((Object.keys(qParameters).length > 2) && (!qParameters.search)) {
        q.nutrients = { $all: matchArr };
      }

      const getFood = await Food.find(q).limit(parseInt(qParameters.length));
      // Creating the response object. 
      const resObj = {};
      resObj.resultCount = getFood.length;
      resObj.data = getFood;
      return res.status(200).json(resObj);
    } catch (e) {
      console.log(e);
      return res.status(400).json(e);
    }
  },
  seedFoodData: (req, res) => {
    return new Promise(async (resolve, reject) => {
      try {
        // In order to use the data I needed to insert the JSON file in the body of this request.
        const query = await Food.insertMany(req.body);
        resolve(res.status(200).json({ success: `You have added ${query.length} food items successfully!` }));
      } catch (e) {
        reject(res.status(400).json({ error: e }));
      }
    });
  },
  test: async (req, res) => {
    try {
      // Default test to see if API is on.
      res.json({ success: "API IS ON" });
    } catch (error) {
      res.json(error);
    }
  }
};
