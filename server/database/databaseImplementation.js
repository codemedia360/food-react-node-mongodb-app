const mongoose = require('mongoose');

// This connects the api to a mongoDB.
mongoose.connection.openUri(process.env.MONGODB_URI)
  .once('open', () => console.log('Mongoose connection successful.'))
  .on('error', (error) => {
    console.warn('Warning', error);
  });