'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// This is a creating the schema of the food collection.
var foodModel = new Schema({
    ndbno: String,
    name: String,
    weight: Number,
    measure: String,
    nutrients: [{
        nutrient_id: String,
        nutrient: String,
        unit: String,
        value: Number,
        gm: Number,
    }],
}, {
    collection: 'Foods'
});

module.exports = mongoose.model('Foods', foodModel); 