'use strict';


const EXPRESS = require('express'),
    ROUTER = EXPRESS.Router();

module.exports = function (app) {
    //***********CONTROLLERS****************************//
    const API_CONTROLLER = require('./controllers/api_controller');

    // |||||||||||||||||||||||||||| FORM ENDPOINTS ||||||||||||||||||||||||||||||||||||||||||||||||||||
    ROUTER.get('/v1/foods', API_CONTROLLER.getFoodData);
    ROUTER.post('/v1/seedFoodData', API_CONTROLLER.seedFoodData);


    // |||||||||||||||||||||||||||| TEST ENDPOINT ||||||||||||||||||||||||||||||||||||||||||||||||||||
    ROUTER.get('/', API_CONTROLLER.test);

    //**************MOUNT ROUTER********************//
    app.use('/api', ROUTER);
}